// basic file operations
#include <iostream>
#include "logfile.h"
#include "Crypto/state.h"

using namespace std;

int main () {

    LogFile logFile = LogFile();
    string key = "Chiave";
    string to_enc = "Lorem ipsum dolor sit amet, con";
    logFile.writeFile("Text to be cripted: " + to_enc + "\n");

    for (unsigned int index = 0; index < to_enc.length(); index += BLOCK_LENGTH/4) {
        state crypt_state(to_enc.substr(0, BLOCK_LENGTH / 4));
        logFile.writeFile(crypt_state.encrypt(key));
    }

    logFile.writeFile("\n");

    return 0;
}

