#ifndef LOG_SYSTEM_LOGFILE_H
#define LOG_SYSTEM_LOGFILE_H

#include <string>

using namespace std;

class LogFile{
    string PATH;
    string FILENAME;


    public:
        LogFile();
        string readConfigPath();
        string readConfigFilename();
        int writeFile(string text);
        int readFile();
};

#endif