#include <fstream>
#include <iostream>
#include "logfile.h"


LogFile::LogFile() {

    PATH = LogFile::readConfigPath();
    FILENAME = LogFile::readConfigFilename();

}

/**
 * Function for read the configuration file and set the path for the log file.
 *
 * @return a string with the path value.
 */
string LogFile::readConfigPath() {

    string param = "PATH=";
    ifstream file;
    string line;
    file.open("config.txt", ios::in);

    if(file.fail()){
        cerr << "Error opening copnfiguration file!\nConfiguration file not in the same folder of .exe\n";
        return "";
    }

    while(getline(file, line)){
        int pos = line.find(param);
        if (pos != string::npos) {
            return line.substr(pos + param.length());
        }
    }

    cerr << "Param :" << param << " not found! Add it in configuration file.\n";
    return "";

}

/**
 * Function for read the configuration file and set the filename for the log file.
 *
 * @return a string with the filename value.
 */
string LogFile::readConfigFilename() {

    string param = "FILENAME=";
    ifstream file;
    string line;
    file.open("config.txt", ios::in);

    if(file.fail()){
        cerr << "Error opening copnfiguration file!\nConfiguration file not in the same folder of .exe\n";
        return "";
    }

    while(getline(file, line)){
        int pos = line.find(param);
        if (pos != string::npos) {
            return line.substr(pos + param.length());
        }
    }

    cerr << "Param :" << param << " not found! Add it in configuration file.\n";
    return "";
}

/**
 * This function is used for write a string into the file.
 * The write is in append mode.
 *
 * @param text -> The string to append to the file
 * @return -> SUCCESS: 0 as value
 *            FAILURE: -1 as value
 */
int LogFile::writeFile(string text) {

    ofstream file;
    file.open(PATH + FILENAME, ios::app);

    if(file.fail()){
        cerr << "Error opening file in append mode!\n";
        return -1;
    }

    file << text;
    file.close();
    return 0;
}


//TODO
int LogFile::readFile() {

    ifstream file;
    file.open(PATH + FILENAME, ios::in);

    if(file.fail()){
        cerr << "Error opening file!";
        return -1;
    }

    return 0;
}